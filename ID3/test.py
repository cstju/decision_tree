# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2017-02-22 15:08:15
# @Last Modified by:   mac@lab538
# @Last Modified time: 2017-02-23 10:04:03

"""
不能直接使用import ID3
原因是ID3在这里是指ID3.py 文件
这个py文件中可能有许多个类，所以必须具体到import哪个类，
或者使用 from ID3 import *
表示把这个文件中的所有类都import 进来
"""
from ID3 import ID3
def readData(filename):
	fr = open(filename)
	inputData = []
	for line in fr:
		arr = line.strip().split('\t')[1:]
		inputData.append(arr)
	return inputData

lenses = readData('lenses.txt')
lensesLabels = ['age','prescript','astigatic','tear rate']
testID3 = ID3()
lensesTree = testID3.createTree(lenses,lensesLabels,0.1)
testID3.storeTree(lensesTree,'Tree.pkl')
print lensesTree