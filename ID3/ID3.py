# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2017-02-22 14:41:24
# @Last Modified by:   mac@lab538
# @Last Modified time: 2017-02-28 10:34:49
from math import log
import operator
import pdb
import cPickle
class ID3(object):
	"""docstring for ID3"""
	def __init__(self):
		pass

	def createDataSet(self):
		dataSet = [[0, 0, 0, 0, 'N'], 
		           [0, 0, 0, 1, 'N'],
		           [0, 1, 0, 1, 'Y'], 
		           [0, 1, 1, 0, 'Y'], 
		           [0, 0, 0, 0, 'N'], 
		           [1, 0, 0, 0, 'N'],  
		           [1, 0, 0, 1, 'N'], 
		           [1, 1, 1, 1, 'Y'], 
		           [1, 0, 1, 2, 'Y'], 
		           [1, 0, 1, 2, 'Y'], 
		           [2, 0, 1, 2, 'Y'], 
		           [2, 0, 1, 1, 'Y'], 
		           [2, 1, 0, 1, 'Y'], 
		           [2, 1, 0, 2, 'Y'], 
		           [2, 0, 0, 0, 'N']]
		labels = ['age','job','house','credit','category']
		return dataSet, labels

	def calcShannonEnt(self,dataset):
		"""
		计算香农熵
		"""
		numEntries = len(dataset)
		labelCounts = {}
		for featVec in dataset:
			currentLable = featVec[-1]
			labelCounts.setdefault(currentLable,0)
			labelCounts[currentLable] += 1
		shannonEnt = 0.0
		for key in labelCounts:
			prob = float(labelCounts[key])/numEntries
			shannonEnt -= prob*log(prob,2)
		return shannonEnt

	def splitDataSet(self,dataset,axis,value):
		"""
		按照某一维度划分数据集
		"""
		retDataSet = []
		for featVec in dataset:
			if featVec[axis]==value:
				reducedFeatVec = featVec[:axis]+featVec[axis+1:]
				retDataSet.append(reducedFeatVec)
		return retDataSet

	def chooseBestFeatureToSplit(self,dataset):
		"""
		选择信息增益最大的特征进行划分
		返回的是该特征的下标编号
		"""
		numFeatures = len(dataset[0])-1
		baseEntropy = self.calcShannonEnt(dataset)#计算初始香农熵
		bestInfoGain = 0.0
		bestFeature = -1#最佳划分数据的特征角标编号
		for i in range(numFeatures):#遍历每个特征
			#获取该特征的所有取值
			uniqueVals = set([example[i] for  example in dataset])
			newEntropy = 0.0
			for value in uniqueVals:
				subDataSet = self.splitDataSet(dataset,i,value)
				prob = float(len(subDataSet))/len(dataset)
				newEntropy += prob*self.calcShannonEnt(subDataSet)
			infoGain = baseEntropy-newEntropy
			if infoGain>bestInfoGain :
				bestInfoGain = infoGain
				bestFeature = i
		return bestFeature,bestInfoGain

	def majorityCnt(self,classList):
		classCount = {}
		for vote in classList:
			classCount.setdefault(vote,0)
			classCount[vote]+=1
		sortedClassCount = sorted(classCount.iteritems(),key=operator.itemgetter(1),reverse=True)
		return sortedClassCount[0][0]#返回统计个数最多的类别

	def createTree(self,dataset,labels,threshold):
		classList = [example[-1] for example in dataset] #获取类别列表
		#如果类别列表长度与其第一个元的个数相同
		#说明这个类别列表中的所有类别一致，返回该类别
		if classList.count(classList[0])==len(classList):
			return classList[0]
		#遍历完所有特征，现在数据集中只剩下类别一列
		#则将该选择该类别中标签最多的那一类作为该类别的分类
		if len(dataset[0])==1:
			return self.majorityCnt(classList)
		bestFeat,bestGain = self.chooseBestFeatureToSplit(dataset)

		#如果信息增益这个阈值，则设为单节点不继续划分
		if bestGain < threshold:
			return self.majorityCnt(classList)

		bestFeatLabel = labels[bestFeat]
		myTree = {bestFeatLabel:{}}
		del(labels[bestFeat])
		uniqueVals = set([example[bestFeat] for example in dataset])
		for value in uniqueVals:
			subLabels = labels[:]
			myTree[bestFeatLabel][value] = self.createTree(self.splitDataSet(dataset,bestFeat,value),subLabels,threshold)
		return myTree

	def getNumLeafs(self,myTree):
		numLeafs = 0
		firstStr = myTree.keys()[0]
		secondDict = myTree[firstStr]
		for key in secondDict.keys():
			#如果不再是字典，说明是叶节点，计数加一
			#如果还是字典，则递归，计算叶节点个数
			if type(secondDict[key]).__name__=='dict':
				numLeafs += self.getNumLeafs(secondDict[key])
			else: numLeafs+=1
		return numLeafs

	def getTreeDepth(self,myTree):
		maxDepth = 0
		firstStr = myTree.keys()[0]
		secondDict = myTree[firstStr]
		for key in secondDict.keys():
			#如果是字典，则说明有深度，采用递归的方法
			if type(secondDict[key]).__name__=='dict':
				thisDepth = 1+self.getTreeDepth(secondDict[key])
			else: thisDepth = 1
			if thisDepth>maxDepth: maxDepth=thisDepth
		return maxDepth

	def classify(self,inputTree,featLabels,testVec):
		firstStr = inputTree.keys()[0]
		secondeDict = inputTree[firstStr]
		#获取该特征在特征列表中的位置
		featIndex=featLabels.index(firstStr)
		for key in secondDict.keys():
			if testVec[featIndex] == key:
				#如果仍是树，说明没有查找到最底层，递归继续查找
				if type(secondeDict[key]).__name__ == 'dict':
					classLabel = self.classify(secondeDict[key],featLabels,testVec)
				#直到查到最底层，返回其类别
				else: classLabel = secondeDict[key]
		return classLabel


	"""
	Python中可以使用 pickle 模块将对象转化为文件保存在磁盘上，
	在需要的时候再读取并还原。具体用法如下：
	
	pickle.dump(obj, file[, protocol])
	这是将对象持久化的方法，参数的含义分别为：
	obj: 要持久化保存的对象；
	
	file: 一个拥有 write() 方法的对象，并且这个 write() 方法能接收一个字符串作为参数。
	这个对象可以是一个以写模式打开的文件对象或者一个 StringIO 对象，或者其他自定义的满足条件的对象。
	
	protocol: 这是一个可选的参数，默认为 0 ，如果设置为 1 或 True，
	则以高压缩的二进制格式保存持久化后的对象，否则以ASCII格式保存。
	
	pickle.load(file)
	只有一个参数 file ，对应于上面 dump 方法中的 file 参数。
	这个 file 必须是一个拥有一个能接收一个整数为参数的 read() 方法以及一个不接收任何参数的
	readline() 方法，并且这两个方法的返回值都应该是字符串。
	这可以是一个打开为读的文件对象、StringIO 对象或其他任何满足条件的对象。
	"""
	def storeTree(self,inputTree,filename):
		#存储已经生成好的决策树
		fw = open(filename,'w')
		cPickle.dump(inputTree,fw,True)
		fw.close()

	def getTree(self,filename):
		fr = open(filename)
		myTree = cPickle.load(fr)
		fr.close()
		return myTree


if __name__ == '__main__':
	test = ID3()
	dataset,labels = test.createDataSet()
	print test.calcShannonEnt(dataset)	
	print test.splitDataSet(dataset,2,1)
	print test.chooseBestFeatureToSplit(dataset)
	#test.createTree(dataset,labels)
	print test.createTree(dataset,labels)
