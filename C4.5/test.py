# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2017-02-22 15:08:15
# @Last Modified by:   mac@lab538
# @Last Modified time: 2017-02-23 17:21:10

from C45 import C45
def readData(filename):
	fr = open(filename)
	inputData = []
	for line in fr:
		arr = line.strip().split('\t')[1:]
		inputData.append(arr)
	return inputData

lenses = readData('lenses.txt')
lensesLabels = ['age','prescript','astigatic','tear rate']
print lensesLabels
testC45 = C45()
lensesTree = testC45.createTree(lenses,lensesLabels,0.1)
#testC45.storeTree(lensesTree,'Tree.pkl')
#tree = testC45.getTree('Tree.pkl')
#print tree
print lensesTree
print lensesLabels
print testC45.cutBranchUpToDown(lensesTree,lenses,lensesLabels)
print testC45.getCount(lensesTree,lenses,lensesLabels)