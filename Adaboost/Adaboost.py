# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2017-02-24 11:23:56
# @Last Modified by:   mac@lab538
# @Last Modified time: 2017-02-25 23:53:55
import numpy as np
import scipy as sp
import cPickle
import pdb
from WeakClassify import WeakClassify
from Tools import sign
class Adaboost:
	def __init__(self,feature,result,weaker=WeakClassify):

		'''
		输入：
		feature：训练集的特征数据，是样本个数x特征个数的矩阵
		result：训练集结果数据，是个数为样本数的向量
		weaker：弱分类器的类名
		'''
		self.trainFeature=feature
		self.trainResult=result
		self.weaker=weaker#确定弱分类器
		#assert断言是声明其布尔值必须为真的判定，如果发生异常就说明表达示为假。
		assert self.trainFeature.shape[0]==self.trainResult.shape[0],'训练集个数不匹配'
		
		self.sums=np.zeros(self.trainResult.shape)#用于判断是否已经完成分类

		"""
		.flatten('C') 是将矩阵按照行转化为一个一行矩阵
		.flatten('F') 是将矩阵按照列转化为一个一行矩阵
		>>> b = numpy.array([[1,2],[3,4]])
		>>> print b.flatten('C')
		[1 2 3 4]
		>>> print b.flatten('F')
		[1 3 2 4]
		"""
		#训练数据的权重(行向量)，并做归一化处理
		self.weights=np.ones(self.trainFeature.shape[0]).flatten('C')/self.trainFeature.shape[1]
		self.finalWeakerNum=0#最终实际使用的弱分类器个数
	def train(self,weakerNum=4):
		'''
		输入：
		weakerNum：弱分类器的个数
		'''
		self.weakerClassifies={}#弱分类器集合
		self.weakerAlpha={}#弱分类器权重
		for i in range(weakerNum):
			self.weakerClassifies.setdefault(i)
			self.weakerAlpha.setdefault(i,1)
		for i in range(weakerNum):
			self.weakerClassifies[i]=self.weaker(self.trainFeature,self.trainResult)

			#误差率，计算误差率时需要当前训练数据的权重
			error=self.weakerClassifies[i].train(self.weights)
			print "当前弱分类器的误差>>>>>>>>>>>: " ,error

			if error == 0.0:
				print self.weights
				print "只采用单一弱分类就可是误差率为0"
				break

			#当前弱分类器的系数
			self.weakerAlpha[i]=float(1.0/2*np.log((1-error)/error))
			print "当前弱分类器的系数>>>>>>: " ,self.weakerAlpha[i]



			#更新训练数据的权值分布
			sg=self.weakerClassifies[i].pred(self.trainFeature)
			Z=self.weights*np.exp(-self.weakerAlpha[i]*self.trainResult*sg)
			self.weights=(Z/Z.sum())
			print "当前训练样本权重>>>>>>>>>>>: " ,self.weights

			self.finalWeakerNum=i
			#print self.finalclassifer(i),'==========='
			
			if self.ifFinalClassifer(i)==0:
				print i+1," 个弱分类已经可使误差率为0"
				break
			
	
	def ifFinalClassifer(self,weakerNum):
		'''
			the 1 to weakerNum weak classifer come together
		'''
		self.sums=self.sums+self.weakerClassifies[weakerNum].pred(self.trainFeature)*self.weakerAlpha[weakerNum]
		print self.sums
		pre_y=sign(self.sums)

		error=(pre_y!=self.trainResult).sum()
		return error
		
	def pred(self,test_set):
		test_set = np.array(test_set)
		print test_set
		assert test_set.shape[1]==self.trainFeature.shape[1],'训练集和测试集特征个数不匹配'
		sums=np.zeros(test_set.shape[0])

		for i in range(self.finalWeakerNum+1):
			print self.weakerAlpha[i]
			sums=sums+self.weakerClassifies[i].pred(test_set)*self.weakerAlpha[i]
		pre_y=sign(sums)
		return pre_y
	
	def storeWeakerClassifies(self,filename):
		fw = open(filename,'w')
		cPickle.dump(self.weakerClassifies,fw,True)
		fw.close()

	def storeWeakerAlpha(self,filename):
		fw = open(filename,'w')
		cPickle.dump(self.weakerAlpha,fw,True)
		fw.close()

	def getWeakerClassifies(self,filename):
		fr = open(filename)
		myTree = cPickle.load(fr)
		fr.close()
		return myTree

	def getWeakerAlpha(self,filename):
		fr = open(filename)
		myTree = cPickle.load(fr)
		fr.close()
		return myTree
