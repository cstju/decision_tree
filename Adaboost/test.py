# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2017-02-24 11:18:50
# @Last Modified by:   mac@lab538
# @Last Modified time: 2017-02-25 23:54:31
from Adaboost import Adaboost
from WeakClassify import WeakClassify
import numpy as np
import scipy as sp
import pdb

"""
trainFeature=np.array([[0.55,4.4],[1.1,2.8],[1.85,1.95],[3.15,1.7],[4,2.7],
			[3.75,3.95],[2.8,4.4],[2.35,3.2],[3.05,2.25],[3.55,2.6],
			[3.1,3],[3,3.4],[1,7.3],[1.4,6.7],[3.05,6.9],
			[4.3,7.15],[4.75,7],[5.5,5.85],[5.95,4.75],[6.45,3.15],
			[6.5,1.35],[6.3,0.95],[5.95,0.85],[5.95,1.6],[5.85,2.75],
			[5.65,4],[5.35,5.25],[5,6.15],[4.7,6.3],[3.85,6.5],
			[2.55,6.55],[1.4,6.65],[0.6,6.75],[0.6,6.85],[5.35,0.9]])

trainResult=np.array([-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
						-1,-1,1,1,1,1,1,1,1,1,
						1,1,1,1,1,1,1,1,1,1,
						1,1,1,1,1])
"""
#pdb.set_trace()
"""
采用itis数据集中的前两个花的品种作为训练样本
"""


def readDataFeature(filename):
	fr = open(filename)
	inputData = []
	for line in fr:
		arr = line.strip().split(',')
		sl = float(arr[0])
		sw = float(arr[1])
		pl = float(arr[2])
		pw = float(arr[3])
		inputData.append([sl,sw,pl,pw])
	return inputData

def readDataResult(filename):
	fr = open(filename)
	inputResult = []
	for line in fr:
		arr = line.strip().split(',')[-1:]
		if arr[0] == 'Iris-setosa':
			inputResult.append(1)
		else:
			inputResult.append(-1)
	return inputResult

inputFeature = np.array(readDataFeature("./dataset/itis.data"))
inputResult = np.array(readDataResult("./dataset/itis.data"))
print inputFeature
print inputResult
a= Adaboost(inputFeature,inputResult,WeakClassify)
#pdb.set_trace()
a.train(5)


print a.pred([[5.8,4.0,1.2,0.2],[5.8,4.0,1.2,0.2],[5.6,3.0,4.5,1.5]])