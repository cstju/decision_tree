#实现Adaboost分类
###1、本程序参考《统计学习方法》第八章提升方法的讲解，和GitHub（https://github.com/justdark/dml/tree/master/dml/ADAB）的内容实现了Adaboost的分类算法
####（1）分类算法可以使用多维输入，但是输出必须是二维（-1或1）。弱分类函数采用的就是普通的二分类（大于某个阈值取1，小于某个阈值取-1，或反过来，大于某个阈值取-1，小于某个阈值取1）
####（2）对于多维输入，每一次弱分类是选择所有输入特征中的一个能将这次弱分类的误差降到最小的特征和对应的弱分类器作为该次的弱分类器和判断特征
	例如：
	如果当前输入特征是3维，结果是-1或者1
	选取第一个弱分类器时，会遍历三个特征，分别得到三个弱分类器和对应的误差率
	再比较三个误差率，最终选择一个误差率最小的特征和其弱分类器作为这次最终的弱分类器
	
###2、接下来要实现Adaboost的回归算法
###3、收藏其他adaboost的项目：
在上层文件夹adaboost参考中增加了三个别人的项目，分别是：

1、[NaiveBayesSpamFilter-master.zip](https://github.com/SunnyMarkLiu/NaiveBayesSpamFilter)

2、[163musicSpider-master.zip](https://github.com/JLModra/163musicSpider)

3、[Advanced-Machine-Learning-master.zip](https://github.com/bt3gl/Advanced-Machine-Learning/tree/master/adaboost)
