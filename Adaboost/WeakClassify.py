# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2017-02-24 11:25:23
# @Last Modified by:   mac@lab538
# @Last Modified time: 2017-02-25 23:34:13
import numpy as np
import scipy as sp
"""
本程序是实现<统计学习方法>中第八章提升方法Adaboost
中分类问题的弱分类器(基本分类器)，
在Adaboost的分类问题中，每一次增加基本分类器是计算误差率
误差率=训练集中错分类个数/训练集总数
"""

class WeakClassify:
	"""
	对于多维输入，每一次弱分类是选择所有输入特征中的一个
	能将这次弱分类的误差降到最小的特征和对应的弱分类器作为该次的弱分类器和特征
	例如：
	如果当前输入特征是3维，结果是-1或者1
	选取第一个弱分类器时，会遍历三个特征，分别得到三个弱分类器和对应的误差率
	再比较三个误差率，最终选择一个误差率最小的特征和其弱分类器作为这次最终的弱分类器
	"""
	def __init__(self,feature,result):
		'''
		输入：
		feature：训练集的特征数据，是样本个数x特征个数的矩阵
		result：训练集结果数据，是样本个数x1的矩阵
		weakerNum：弱分类器的个数
		'''
		self.trainFeature=feature
		self.trainResult=result
		self.featureNum=self.trainFeature.shape[1]
	def train(self,weights,steps=100):
		'''
		输入：
		weights：每一个训练样本的权重，是样本个数x1的矩阵
		steps：将数据集划分的步长
		'''
		minError = 100000000000.0#将误差率初始为一个很大的值
		thresholdFinalValue=0;#每一个弱分类最终的阈值
		thresholdFinalFeatureId=0;#每一次弱分类器选择的特征id
		finalValueLessThreshold=0;#小于阈值时应取的值(取值为-1或者1)
		self.weights=weights
		for featureId in range(self.featureNum):
			thresholdValue,error = self.findMinError(featureId,1,steps)
			if (error<minError):
				minError = error
				thresholdFinalValue = thresholdValue
				thresholdFinalFeatureId = featureId
				finalValueLessThreshold = 1
		for featureId in range(self.featureNum):
			thresholdValue,error = self.findMinError(featureId,-1,steps)
			if (error<minError):
				minError = error
				thresholdFinalValue = thresholdValue
				thresholdFinalFeatureId = featureId
				finalValueLessThreshold = -1
		self.thresholdFinalValue=thresholdFinalValue
		self.thresholdFinalFeatureId=thresholdFinalFeatureId
		self.finalValueLessThreshold=finalValueLessThreshold
		return minError

	def findMinError(self,featureId,ValueLessThreshold,steps):
		buttom=np.min(self.trainFeature[:,featureId])#获取特征i的最小取值
		up=np.max(self.trainFeature[:,featureId])#获取特征i的最小取值
		minError=1000000
		minThreshold=0
		stepThreshold=(up-buttom)/steps#阈值步长
		#error = 0
		for threshold in np.arange(buttom,up,stepThreshold):
			now = self.predictInTrain(self.trainFeature,featureId,threshold,ValueLessThreshold)
			error = 0
			for i in range(now.shape[0]):
				error += np.abs((now[i]-self.trainResult[i]))/now.shape[0]*self.weights[i]
			if error<minError:
				minError=error
				minThreshold=threshold
		print '第：',featureId,'个特征的阈值为：',minThreshold,'时，误差为：',minError
		#print minThreshold,minError
		return minThreshold,minError

	"""
	predictInTrain计算给定特征id和阈值情况下，训练集的预测结果
	"""
	def predictInTrain(self,trainSet,featureId,threshold,ValueLessThreshold):
		divisionResult = np.ones(np.array(trainSet).shape[0])
		for trainid in range(trainSet.shape[0]):
			if trainSet[trainid][featureId]*ValueLessThreshold<threshold*ValueLessThreshold:
				divisionResult[trainid] = -1
		return divisionResult

	def pred(self,testset):
		#test_set=np.array(test_set).reshape(self.N,-1)
		t = np.ones(testset.shape[0])
		for i in range(testset.shape[0]):
			if (testset[i][self.thresholdFinalFeatureId]*self.finalValueLessThreshold<
				self.thresholdFinalValue*self.finalValueLessThreshold):
				t[i] = -1
		return t