# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2017-02-24 11:27:16
# @Last Modified by:   mac@lab538
# @Last Modified time: 2017-02-25 17:57:57
import numpy as np
import scipy as sp
def sign(x):
	q=np.zeros(np.array(x).shape)
	q[x>=0]=1
	q[x<0]=-1
	return q