# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2017-02-27 15:04:32
# @Last Modified by:   mac@lab538
# @Last Modified time: 2017-03-02 21:40:51

from collections import defaultdict
import numpy
import math
import pdb

class TreeNode(object):
	def  __init__(self,**kwargs):
		"""
		featureId:特征id
		featureValue:特征值
		result:结果值
		leftBranch:左子树
		rightBranch:右子树
		"""
		"""
		*args表示任何多个无名参数，它是一个tuple；
		**kwargs表示关键字参数，它是一个dict。
		并且同时使用*args和**kwargs时，必须*args参数列要在**kwargs前
		"""
		self.featureId = kwargs.get('featureId')#获取关键字为featureId的值
		self.featureValue = kwargs.get('featureValue')
		self.result = kwargs.get('result')
		self.leftBranch = kwargs.get('leftBranch')
		self.rightBranch = kwargs.get('rightBranch')

class CARTRegression(object):
	"""本类实现的是CART回归树"""
	def __init__(self):
		self.root = None

	def calSquareError(self,dataset):
		'''
		:param dataset:待计算数据集
		:return: squareError:该数据集的平方误差
		计算给定数据集的结果均值和平方误差
		'''
		if len(dataset)==0:
			return 0.0
		resultList = [float(example[-1]) for example in dataset]
		resultAverage = numpy.mean(resultList)
		totalError = 0.0
		for result in resultList:
			totalError += (result-resultAverage)**2
		squareError = math.sqrt(totalError)
		return squareError

	def calSquareErrorFeatureId(self,dataset,featureId,featureType):
		'''
		:param dataset:训练集的一个子集
		:param featureId:特征id，第i个特征
		:param featureType:特征类型数组,True-连续型;False-离散型
		:return finalFeatureValue:以这个特征进行划分的最佳取值
		:return finalFeatureSquErr:以这个特征进行划分的最小平方误差
		根据给定的特征id遍历该特征的所有可能取值，
		划分数据得到平方误差最小的特征值和对应的平方误差
		'''
		datasetDict = defaultdict(list)
		for example in dataset:
			'''
			根据featureId的取值建立字典，
			datasetDict的关键词是featureId所有可能的取值
			值是该取值下的所有训练样本
			'''
			datasetDict[example[featureId]].append(example)
		featureSquErr = {}
		
		for featureValue,featureData in datasetDict.items():
			'''
			featureValue是featureId所有可能的取值
			featuredata是该featureValue对应的样本
			'''
			leftDataset = []
			rightDataset = []
			if featureType[featureId]:#如果该特征是连续型的
				for featureValue1,featureData1 in datasetDict.items():
					if featureValue1<=featureValue:
						leftDataset.extend(featureData1)
					else:
						rightDataset.extend(featureData1)
			else:
				for featureValue1,featureData1 in datasetDict.items():
					if featureValue1==featureValue:
						leftDataset.extend(featureData1)
					else:
						rightDataset.extend(featureData1)
			featureSquErrValue = self.calSquareError(leftDataset)+\
									 self.calSquareError(rightDataset)
			featureSquErr[featureValue] = featureSquErrValue

		finalFeatureValue = min(featureSquErr,key=featureSquErr.get)
		finalFeatureSquErr = featureSquErr[finalFeatureValue]
		return finalFeatureValue,finalFeatureSquErr

	def chooseBestFeatureToSplit(self,dataset,featureIdSet,featureType):
		'''
		:param dataset:待切分的数据集
		:param featureIdSet:可供作为切分特征的特征id集合
		:param featureType:特征类型数组,True-连续型;False-离散型
		:return: bestSplitFeatureId:最佳切分特征id
		:return: bestSplitFeatureValue:最佳切分特征值
		根据给定的特征集合选择最佳特征进行划分
		'''
		bestFeatureDict = {}
		for featureId in featureIdSet:
			bestFeatureDict[featureId] = (self.calSquareErrorFeatureId(dataset,featureId,featureType))
		bestSplitFeatureId = min(bestFeatureDict,key = lambda x: bestFeatureDict.get(x)[1])
		bestSplitFeatureValue = bestFeatureDict[bestSplitFeatureId][0]
		return bestSplitFeatureId,bestSplitFeatureValue

	def splitDataset(self,dataset,featureId,featureValue,featureType):
		'''
		:param dataset:待分割的数据集（可能本身已经是一个子集）
		:param featureId:特征编号
		:param featureValue:特征值
		:param featureType:特征类型数组,True-连续型;False-离散型
		:return: leftDataset:左子集
		:return: rightDataset:右子集
		根据指定的特征id和特征值将原数据集划分左子集和右子集
		当前划分的特征为连续型
			左子集：子集中所有特征id为featureId的样本其的特征值均小于等于featureValue
			右子集：子集中所有特征id为featureId的样本其的特征值均大于featureValue
		当前划分的特征为离散型
			左子集：子集中所有特征id为featureId的样本其的特征值均为featureValue
			右子集：子集中所有特征id为featureId的样本其的特征值均不为featureValue
		'''
		leftDataset = []
		rightDataset = []
		for example in dataset:
			if featureType[featureId]:
				if example[featureId] <= featureValue:
					leftDataset.append(example)
				else:
					rightDataset.append(example)
			else:
				if example[featureId] == featureValue:
					leftDataset.append(example)
				else:
					rightDataset.append(example)
		return numpy.array(leftDataset),numpy.array(rightDataset)

	def leafMeanValue(self,dataset):
		resultList = [example[-1] for example in dataset]
		return numpy.mean(resultList)

	def buildTree(self,dataset,featureIdSet,featureType):
		'''
		:param dataset:给定构建决策树的数据集(可能是原始数据集也可能是某子集)
		:param featureIdSet:可以进行划分的特征id集合
		:param featureType:特征类型数组,True-连续型;False-离散型
		:return 一个决策树节点
		递归构建决策树
		'''
		resultList = [example[-1] for example in dataset]#获取标签列表
		#if resultList==[]:
		#	return TreeNode()
		if resultList.count(resultList[0]) == len(resultList):#标签列表中只有一种标签
			print "当前数据集结果全部一致，结果为：",resultList[0]
			return TreeNode(result=resultList[0])#将该标签作为叶节点的标签

		#特征id集合集合中已经没有特征可以用于划分，只能是叶节点
		#对当前所有样本的结果取平均值作为该叶节点的结果
		if not featureIdSet:
			finalresultValue = self.leafMeanValue(dataset)
			print "当前数据集已无可划分特征,选择平均值作为该叶节点结果：",finalresultValue
			return TreeNode(result=finalresultValue)

		#选择当前数据集和可划分标签集，调用chooseBestFeatureToSplit函数进行最优划分
		bestSplitFeatureId,bestSplitFeatureValue = self.chooseBestFeatureToSplit(dataset,featureIdSet,featureType)
		leftDataset,rightDataset = self.splitDataset(dataset,bestSplitFeatureId,bestSplitFeatureValue,featureType)


		#如果左子集为空，则由右子集确定叶节点，反之亦然
		if leftDataset.shape[0] == 0:
			return TreeNode(label=self.leafMeanValue(rightDataset))
		elif rightDataset.shape[0] == 0:
			return TreeNode(label=self.leafMeanValue(leftDataset))

		#从可进行划分的特征id集合中删除当前用于划分的特征id
		newFeatureIdSet = featureIdSet-set([bestSplitFeatureId])

		leftBranch = self.buildTree(leftDataset,newFeatureIdSet,featureType)
		rightBranch = self.buildTree(rightDataset,newFeatureIdSet,featureType)
		return TreeNode(leftBranch=leftBranch,rightBranch=rightBranch,
						featureId=bestSplitFeatureId,featureValue=bestSplitFeatureValue)

	def fit(self,train_x,train_y,featureType):
		'''
		:param train_x:训练数据的特征集
		:param train_y:训练数据的标签集
		:param featureType:特征类型数组,True-连续型;False-离散型
		:return：None
		拟合决策树
		'''
		"""
		numpy.c_ = <numpy.lib.index_tricks.CClass object at 0x41611b8c>
		Translates slice objects to concatenation along the second axis.
		>>> np.c_[np.array([[1,2,3]]), 0, 0, np.array([[4,5,6]])]
		array([[1, 2, 3, 0, 0, 4, 5, 6]])
		>>> np.c_[np.array([[1,2,3],[4,5,6]]), np.array([1,2])]
		array([[1, 2, 3, 1],
		       [4, 5, 6, 2]])
		"""
		featureIdSet = set(range(train_x.shape[1]))
		self.train_x = numpy.c_[train_x,train_y]
		self.root = self.buildTree(self.train_x,featureIdSet,featureType)

	def predictOne(self,testExample,featureType):
		'''
		:param testExample:待预测的样本
		:param featureType:特征类型数组,True-连续型;False-离散型
		:return: 该样本的预测类别
		预测单个样本
		'''
		predictNode = self.root
		#如果当前节点的标签为None，说明不是叶节点，向下搜索
		while predictNode.result == None:
			if featureType[predictNode.featureId]:#连续型
				#如果当前样本的特征值小于节点特征值，则划分到左子树，否则划分到右子树
				if testExample[predictNode.featureId]<=predictNode.featureValue:
					predictNode = predictNode.leftBranch
				else:
					predictNode = predictNode.rightBranch
			else:#离散型
				if testExample[predictNode.featureId]==predictNode.featureValue:
					predictNode = predictNode.leftBranch
				else:
					predictNode = predictNode.rightBranch
		#返回叶节点的标签
		return predictNode.result

	def predict(self,testDataset,featureType):
		'''
		:param testDataset:测试数据集
		:param featureType:特征类型数组,True-连续型;False-离散型
		:return: 测试数据集的预测标签一维数组
		'''
		return numpy.array([self.predictOne(example,featureType) for example in testDataset])


		