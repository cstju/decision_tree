#实现CART算法
==============
###CART回归树(CARTRegression.py)
<font size=3>回归树采用平方误差作为划分标准。</font>

<font size=3>
回归树最终的叶节点的值是构建树时划分到该节点所有样本结果的均值。

本程序实现输入特征可以是：1、离散值  2、连续值  3、二者都有
增加一个一维数组featureType，元素个数与特征个数相同，里面的取值为True或者False。

如果为True说明对应的特征是连续值；如果为False对应的特征是离散值。

1. 对于连续型特征，划分时小于等于特征值为一组，大于特征值为一组
2. 对于离散型特征，划分时等于特征值为一组，不等于特征值为一组。
</font>
---------------------------------

###CART分类树(CARTClassification.py)
<font size=3>分类树采用基尼指数作为划分标准</font>

<font size=3>
分类树最终的叶节点的值是构建树时划分到该节点所有样本标签最多的一类。

本程序实现输入特征可以是：1、离散值  2、连续值  3、二者都有
增加一个一维数组featureType，元素个数与特征个数相同，里面的取值为True或者False。

如果为True说明对应的特征是连续值；如果为False对应的特征是离散值。

1. 对于连续型特征，划分时小于等于特征值为一组，大于特征值为一组
2. 对于离散型特征，划分时等于特征值为一组，不等于特征值为一组。
</font>
---------------------------------
###当前疑问：
1. 在程序中，buildTree函数进行树构建时，

		newFeatureIdSet = featureIdSet-set([bestSplitFeatureId])
		leftBranch = self.buildTree(leftDataset,newFeatureIdSet,featureType)
		rightBranch = self.buildTree(rightDataset,newFeatureIdSet,featureType)
   
   可能出现左子树的划分特征也用于右子树划分的情况，CART中每个特征最多只能用于一次划分？