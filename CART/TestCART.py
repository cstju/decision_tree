# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2017-02-27 14:46:03
# @Last Modified by:   mac@lab538
# @Last Modified time: 2017-03-01 15:00:16


from CARTClassification import CARTClassification
from CARTRegression import CARTRegression
import numpy as np


train_class_x = [
		[1.5, 'room', 'bad'],
		[1.25, 'room', 'bad'],
		[1.35, 'noroom', 'bad'],
		[0.5, 'noroom', 'bad'],
		[0.65, 'noroom', 'cool'],
	]
train_class_x = np.array(train_class_x)
train_class_y = np.array(['do', 'do', 'do', 'nodo', 'nodo'])
train_class_type = np.array([True,False,False])
x = train_class_x[4]
clf = CARTClassification()
clf.fit(train_class_x, train_class_y,train_class_type)
print(clf.predictOne(x,train_class_type))
'''
train_regres_x = [
		[1.5, 1.22, 'bad',1.67],
		[1.25, 1.32, 'bad',1.11],
		[1.35, 0.62, 'bad',1.32],
		[0.5, 0.57, 'cool',0.64],
		[0.65, 0.78, 'cool',0.56],
	]
train_regres_x = np.array(train_regres_x)
train_regres_y = np.array([1.23, 1.34, 1.32, 0.54, 0.67])
train_regres_type = np.array([True,True,False,True])
x = train_regres_x[4]
clf = CARTRegression()
clf.fit(train_regres_x, train_regres_y,train_regres_type)
print(clf.predictOne(x,train_regres_type))
'''