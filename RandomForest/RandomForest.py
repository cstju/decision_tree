# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2017-03-02 22:16:26
# @Last Modified by:   mac@lab538
# @Last Modified time: 2017-03-03 11:56:56
from DecisionTree import *
import multiprocessing
import time
import numpy
import random
from math import log
from collections import defaultdict

class RandomForest(object):
	def __init__(self):
		self.root = None
		self.oneTree = DecisionTree()
		self.subDatasets = []
		self.subFeatureIdSets = []
		self.subFeatureIdNum = None#子特征集中特征数

	def randomSplitDataset(self,dataset,setNum):
		'''
		:param: dataset:待提取样本的数据集
		:param: setNum:生成数据集的个数
		根据输入数据集，有放回的提取样本生成新的数据集，新数据集的样本个数与原数据集相同
		'''
		
		for num in range(setNum):
			rowNum = dataset.shape[0]
			#从行总数中有放回的提取rowNum个样本id
			indexs = numpy.random.choice(rowNum,rowNum)
			self.subDatasets.append(dataset.copy()[indexs])

	def randomSplitFeatureSet(self,featureSet,setNum):
		'''
		:param: featureSet:待提取样本的特征id集
		:param: setNum:生成数据集的个数
		根据输入特征id集，有放回的提取样本生成新的特征集，新特征集的个数为log2(特征总数)
		每个子特征集中特征元素不能重复，子特征集的特征个数为log2(特征总数)
		'''
		self.subFeatureIdNum = int(log(len(featureSet),2))
		for num in range(setNum):
			subFeatureIdSets = set(random.sample(featureSet,self.subFeatureIdNum))
			self.subFeatureIdSets.append(subFeatureIdSets)

	def createRandomForest(self):
		self.randomForest = []
		start_time = time.time()
		for datasetId in range(len(self.subDatasets)):
			subset = self.subDatasets[datasetId]
			subFeatureIdSet = self.subFeatureIdSets[datasetId]
			self.randomForest.append(self.oneTree.buildTree(subset,subFeatureIdSet))
		print("--- 花费 %s 秒创建随机森林 ---" % (time.time() - start_time))

	def createRandomForestMultiprocessing(self):
		#定义一个输出队列
		output = multiprocessing.Queue()
		def seedTree(subDataset,subFeatureIdSet,output):
			output.put(self.oneTree.buildTree(subDataset,subFeatureIdSet))
		processes = []
		for datasetId in range(len(self.subDatasets)):
			processes.append(multiprocessing.Process(target=seedTree, args=(self.subDatasets[datasetId], 
				self.subFeatureIdSets[datasetId],output)))
		start_time = time.time()
		for p in processes: p.start()
		# Exit the completed processes
		for p in processes: p.join()
		print("--- 花费 %s 秒创建随机森林(多线程) ---" % (time.time() - start_time))
		# Get process results from the output queue
		self.randomForestMultiprocessing = [output.get() for p in processes]
 

	def predictOne(self,testExample):
		'''
		:param testExample: 测试样本
		:return: 测试样本的类别
		预测单个样本
		'''
		count = defaultdict(int)
		for tree in self.randomForest:
			label = self.oneTree.predictOneInOneTree(tree,testExample)
			count[label] += 1
		return max(count, key=count.get)

	def predictOneMultiprocessing(self,testExample):
		'''
		:param testExample: 测试样本
		:return: 测试样本的类别
		采用异步的方法预测单个样本
		'''
		output = multiprocessing.Queue()
		def classify(tree,testExample2,output):
			output.put(self.oneTree.predictOneInOneTree(tree,testExample2))
		processes = []
		for tree in self.randomForestMultiprocessing:
			processes.append(multiprocessing.Process(target=classify, args=(tree, 
				testExample,output)))
		for p in processes: p.start()
		# Exit the completed processes
		for p in processes: p.join()
		
		# Get process results from the output queue
		count = defaultdict(int)
		for p in processes:
			count[output.get()] += 1
		
		return max(count, key=count.get)

	def predict(self, testDataset):
		'''
		:param testDataset: 测试集
		:return: 测试集样本的类别集合
		预测多个样本
		'''
		start_time = time.time()
		resultlist =  numpy.array([self.predictOne(example) for example in testDataset])
		print("--- 花费 %s 预测结果---" % (time.time() - start_time))
		return resultlist


	def predictMultiprocessing(self, testDataset):
		'''
		:param testDataset: 测试集
		:return: 测试集样本的类别集合
		异步预测多个样本
		'''
		start_time = time.time()
		resultlist = numpy.array([self.predictOneMultiprocessing(example) for example in testDataset])
		print("--- 花费 %s 预测结果(多线程) ---" % (time.time() - start_time))
		return resultlist

