# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2017-03-02 15:13:07
# @Last Modified by:   mac@lab538
# @Last Modified time: 2017-03-03 10:17:43
import pandas
import random
import collections 
from sklearn import cross_validation as cv
import numpy

class Dataset(object):
	def __init__(self):
		pass
	def loadDataset(self,filename,splitRatio):
		'''
		:param filename:数据集目录
		:return trainDataset:数组化的训练集
		:return testDataset:数组化的测试集
		将数据文件读取并按照一定比例随机划分为训练集和测试集
		'''
		datasetFrame = pandas.read_csv(filename,sep = ',')
		trainDatasetFrame,testDatasetFrame = cv.train_test_split(datasetFrame,test_size=splitRatio)
		trainDataset = numpy.array(trainDatasetFrame)
		testDataset = numpy.array(testDatasetFrame)
		featureSet = set(range(trainDataset.shape[1]))
		return trainDataset,testDataset,featureSet

	def cleanDataset(self,dataset):
		'''
		:param dataset:待清理的数据集
		:return afterCleanDataset
		'''
		beforeCleanDataset = dataset.copy()
		afterCleanDataset = dataset.copy()
		for row in range(beforeCleanDataset.shape[0]):
			for column in range(beforeCleanDataset.shape[1]):
				if beforeCleanDataset[row][column]=="":#如果有缺省值，删除该样本
					numpy.delete(afterCleanDataset,row,0)
		return afterCleanDataset

	def randomSplitDataset(self,dataset):
		'''
		:param: dataset:待提取样本的数据集
		:param: subsetNum:生成数据集的个数
		:return newDataset:提取样本生成新的数据集，
		根据输入数据集，有放回的提取样本生成新的数据集，新数据集的样本个数与原数据集相同
		'''
		rowNum = dataset.shape[0]
		#从行总数中有放回的提取rowNum个样本id
		indexs = numpy.random.choice(rowNum,rowNum)
		return dataset.copy()[indexs]
			




if __name__ == '__main__':
	data = Dataset()
	train = data.loadDataset('./training_data/wine-color.csv',0.25)[0]
	print type(train)
	#print train.shape()
	print train
	a = numpy.array([0,0,0])
	print type(a)