# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2017-03-03 10:07:33
# @Last Modified by:   mac@lab538
# @Last Modified time: 2017-03-03 11:49:05
from Dataset import Dataset
from RandomForest import RandomForest
dataset = Dataset()
filename = './training_data/wine-color.csv'
resultTuple= dataset.loadDataset(filename,0.25)
trainDataset = resultTuple[0]
testDataset = resultTuple[1]
featureSet = resultTuple[2]
randomforest = RandomForest()
randomforest.randomSplitDataset(trainDataset,5)
randomforest.randomSplitFeatureSet(featureSet,5)
randomforest.createRandomForest()
randomforest.createRandomForestMultiprocessing()

randomforest.predict(testDataset)
randomforest.predictMultiprocessing(testDataset)
