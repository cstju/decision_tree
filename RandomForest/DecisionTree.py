# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2017-03-02 17:08:43
# @Last Modified by:   mac@lab538
# @Last Modified time: 2017-03-03 11:47:26
from collections import defaultdict
import numpy
from math import log

class TreeNode(object):
	"""决策树节点"""

	def __init__(self, **kwargs):
		'''
		attr_index: 属性编号
		attr: 属性值
		label: 类别（y）
		left_chuld: 左子结点
		right_child: 右子节点
		'''
		self.featureId = kwargs.get('featureId')#获取关键字为featureId的值
		self.featureValue = kwargs.get('featureValue')
		self.label = kwargs.get('label')
		self.leftBranch = kwargs.get('leftBranch')
		self.rightBranch = kwargs.get('rightBranch')

class DecisionTree(object):
	"""采用C4.5作为单决策树算法"""
	def __init__(self):
		self.root = None

	def calEntropy(self,dataset):
		'''
		:param datset:待计算信息熵的数据集
		:return :该数据集的信息熵
		计算香农熵
		'''
		labelCounts = defaultdict(int)
		for example in dataset:
			labelCounts[example[-1]] += 1
		shannonEnt = 0.0
		for key in labelCounts:
			prob = float(labelCounts[key])/len(dataset)
			shannonEnt -= prob*log(prob,2)
		#print shannonEnt
		return shannonEnt

	def calEntropyId(self,dataset,featureId):
		'''
		:param dataset:训练集的一个子集
		:param featureId:特征id，第i个特征
		:return: 该特征id的特征值，和以该值进行划分的信息增益比
		返回给定特征id下的最优切分值和该切分值对应的信息增益比
		'''
		datasetEntropy = self.calEntropy(dataset)#数据集自身的信息熵
		datasetDict = defaultdict(list)
		for example in dataset:
			'''
			根据featureId的取值建立字典，
			datasetDict的关键词是featureId所有可能的取值
			值是该取值下的所有训练样本
			'''
			datasetDict[example[featureId]].append(example)

		datasetFeatureIdEntropy = 0.0#根据特征id划分数据集的信息熵
		for featureValue,featureData in datasetDict.items(): 
			prob = float(len(featureData))/len(dataset)
			datasetFeatureIdEntropy -= prob*log(prob,2)
		if datasetFeatureIdEntropy==0.0:#修复溢出错误
			datasetFeatureIdEntropy = -0.999*log(0.999,2) - 0.001*log(0.001,2)

		featureEntropyGainDict = {}
		if isinstance(featureValue,int) or isinstance(featureValue,float):
			#split_function对特征值进行判断，返回True或False
			split_function=lambda example:example[featureId]<=featureValue
		else:
			split_function=lambda example:example[featureId]==featureValue

		for featureValue,featureData in datasetDict.items(): 
			'''
			featureValue是featureId所有可能的取值
			featuredata是该featureValue对应的样本
			'''
			# Divide the examples into two Datasets and return them
			leftDataset=[example for example in dataset if split_function(example)]
			rightDataset=[example for example in dataset if not split_function(example)]

			leftDatasetEntropy = len(leftDataset)/float(len(dataset))*self.calEntropy(leftDataset)
			rightDatasetEntropy = len(rightDataset)/float(len(dataset))*self.calEntropy(rightDataset)

			featureEntropyValue = datasetEntropy-leftDatasetEntropy-rightDatasetEntropy#计算信息增益

			featureEntropyGainDict[featureValue]=featureEntropyValue/datasetFeatureIdEntropy#计算信息增益比

		finalFeatureEntropyGain = max(featureEntropyGainDict,key=featureEntropyGainDict.get)

		return finalFeatureEntropyGain,featureEntropyGainDict[finalFeatureEntropyGain]


	def chooseBestFeatureToSplit(self,dataset,featureIdSet):
		'''
		:param dataset:待切分的数据集
		:param featureIdSet:可供作为切分特征的特征id集合
		:return:最佳切分特征id和特征值
		根据给定的特征集合选择最佳特征进行划分
		'''
		bestFeatureDict = {}
		for featureId in featureIdSet:
			bestFeatureDict[featureId] = (self.calEntropyId(dataset,featureId))
		#选择信息增益比指数最大的特征对应的特征id
		bestSplitFeatureId = max(bestFeatureDict,key = lambda x: bestFeatureDict.get(x)[1])
		bestSplitFeatureValue = bestFeatureDict[bestSplitFeatureId][0]
		return bestSplitFeatureId,bestSplitFeatureValue

	def splitDataset(self,dataset,featureId,featureValue):
		'''
		:param dataset:待分割的数据集（可能本身已经是一个子集）
		:param featureId:特征编号
		:param featureValue:特征值
		:return:两个数据集,分别为左子集和右子集
		根据指定的特征id和特征值将原数据集划分左子集和右子集
		当前划分的特征为连续型
			左子集：子集中所有特征id为featureId的样本其的特征值小于等于featureValue
			右子集：子集中所有特征id为featureId的样本其的特征值大于featureValue
		当前划分的特征为离散型
			左子集：子集中所有特征id为featureId的样本其的特征值均为featureValue
			右子集：子集中所有特征id为featureId的样本其的特征值均不为featureValue
		'''
		leftDataset = []
		rightDataset = []

		if isinstance(featureValue,int) or isinstance(featureValue,float):
			#split_function对特征值进行判断，返回True或False
			split_function=lambda example:example[featureId]<=featureValue
		else:
			split_function=lambda example:example[featureId]==featureValue

		for example in dataset:
			leftDataset=[example for example in dataset if split_function(example)]
			rightDataset=[example for example in dataset if not split_function(example)]
		return numpy.array(leftDataset),numpy.array(rightDataset)

	def vote(self,dataset):
		'''
		:param dataset:
		投票选择当前数据集所占比重最大的类别
		'''
		labelNum = defaultdict(int)
		for example in dataset:
			labelNum[example[-1]] += 1
		return max(labelNum,key=labelNum.get)

	def buildTree(self,dataset,featureIdSet):
		'''
		:param dataset:给定构建决策树的数据集(可能是原始数据集也可能是某子集)
		:param featureIdSet:可以进行划分的特征id集合
		:return 一个决策树节点
		递归构建决策树
		'''
		labelList = [example[-1] for example in dataset]#获取标签列表
		if labelList.count(labelList[0]) == len(labelList):#标签列表中只有一种标签
			#print "当前数据集标签全部一致，标签为：",labelList[0]
			return TreeNode(label=labelList[0])#将该标签作为叶节点的标签

		#特征id集合集合中已经没有特征可以用于划分，只能是叶节点
		#选择当前数据集中最多的标签作为该叶节点的标签
		if not featureIdSet:
			finallabelValue = self.vote(dataset)
			#print "当前数据集已无可划分特征,选择出现次数最多的标签作为该叶节点标签标签：",finallabelValue
			return TreeNode(label=finallabelValue)

		#选择当前数据集和可划分标签集，调用chooseBestFeatureToSplit函数进行最优划分
		bestSplitFeatureId,bestSplitFeatureValue = self.chooseBestFeatureToSplit(dataset,featureIdSet)

		leftDataset,rightDataset = self.splitDataset(dataset,bestSplitFeatureId,bestSplitFeatureValue)
		
		#如果左子集为空，则由右子集确定叶节点，反之亦然
		if leftDataset.shape[0] == 0:
			return TreeNode(label=self.vote(rightDataset))
		elif rightDataset.shape[0] == 0:
			return TreeNode(label=self.vote(leftDataset))

		#从可进行划分的特征id集合中删除当前用于划分的特征id
		newFeatureIdSet = featureIdSet-set([bestSplitFeatureId])

		leftBranch = self.buildTree(leftDataset,newFeatureIdSet)
		rightBranch = self.buildTree(rightDataset,newFeatureIdSet)
		return TreeNode(leftBranch=leftBranch,rightBranch=rightBranch,
						featureId=bestSplitFeatureId,featureValue=bestSplitFeatureValue)

	def predictOneInOneTree(self,tree,testExample):
		'''
		:param: tree:树对象
		:param: testexample:测试样本
		:return: 该测试样本的类别
		'''
		predictNode = tree
		while predictNode.label == None:
			continuous=isinstance(testExample[predictNode.featureId],int) or isinstance(testExample[predictNode.featureId],float)
			if continuous:
				if testExample[predictNode.featureId]<=predictNode.featureValue:
					predictNode = predictNode.leftBranch
				else:
					predictNode = predictNode.rightBranch
			else:
				if testExample[predictNode.featureId]==predictNode.featureValue:
					predictNode = predictNode.leftBranch
				else:
					predictNode = predictNode.rightBranch
		return predictNode.label

if __name__ == '__main__':
	my_data=[['slashdot','USA','yes',18,'None'],
			['google','France','yes',23,'Premium'],
			['digg','USA','yes',24,'Basic'],
			['kiwitobes','France','yes',23,'Basic'],
			['google','UK','no',21,'Premium'],
			['(direct)','New Zealand','no',12,'None'],
			['(direct)','UK','no',21,'Basic'],
			['google','USA','no',24,'Premium'],
			['slashdot','France','yes',19,'None'],
			['digg','USA','no',18,'None'],
			['google','UK','no',18,'None'],
			['kiwitobes','UK','no',19,'None'],
			['digg','New Zealand','yes',12,'Basic'],
			['slashdot','UK','no',21,'None'],
			['google','UK','yes',18,'Basic'],
			['kiwitobes','France','yes',19,'Basic']]
	tree = DecisionTree()
	print tree.calEntropyId(my_data,1)
	print tree.splitDataset(my_data,3,19.5)
	print tree.buildTree(my_data,set([0,1,2,3]))





